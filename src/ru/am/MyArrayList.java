package ru.am;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;

public class MyArrayList<E> extends AbstractList<E> {

    private final int defaultSize = 10;
    private int size;
    private int sizeFilled;
    private Object[] array;

    // Обычное обьявление
    public MyArrayList() {
        size = 10;
        sizeFilled = 0;
        array = new Object[defaultSize];
    }

    // Обьявление определенного размера
    public MyArrayList(int length) {
        size = length;
        sizeFilled = 0;
        array = new Object[length];
    }

    // Обьявление c передачей
    public MyArrayList(Collection<? extends E> array){
        size = array.size();
        sizeFilled = array.size();
        this.array = array.toArray();
    }

    // размер
    public int size() {
        return sizeFilled;
    }

    // проверка на пустоту
    public boolean isEmpty() {
        return sizeFilled == 0;
    }

    // проверка на наличие элемента по первому входжению
    public boolean contains(Object o) {
        return Arrays.stream(array)
                .anyMatch(a -> a.equals(o));
    }

    // добавление элемента
    public boolean add(Object o) {
        int newSizeFilled = sizeFilled + 1;
        if (newSizeFilled > size) {
            Object[] newArray = new Object[size*3/2+1];
            System.arraycopy(array, 0, newArray, 0, sizeFilled);
            newArray[newSizeFilled-1] = o;
            array = newArray;
            size = array.length;
        } else {
            array[sizeFilled] = o;
        }
        sizeFilled++;
        return true;
    }

    // удаление элемента
    public boolean remove(Object o) {
        for (int i = 0; i < sizeFilled; i++) {
            if (o.equals(array[i])) {
                array[i] = null;
                return true;
            }
        }
        return false;
    }

    // получение элемента по индексу
    public E get(int index) {
        return index < sizeFilled ? (E) array[index] : null;
    }

    // добавление элемента по индексу
    public void add(int index, Object o) {
        if (index <= sizeFilled) array[index] = o;
    }

    // удаление элемента по индексу
    public E remove(int index) {
        Object removed;
        if (index < sizeFilled) {
            removed = array[index];
            array[index] = null;
            return (E) removed;
        }
        return null;
    }

    // получение индека элемента
    public int indexOf(Object o) {
        for (int i = 0; i < sizeFilled; i++) {
            if (o.equals(array[i])) {
                return i;
            }
        }
        return -1;
    }

}
